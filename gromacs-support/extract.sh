cd data
for year in 2019 2020; do
for month in January February March April May June July August September October November December;do
for name in abraham bauer blau lindahl pall villa hess jordan zhmurov; do
  num=$(grep -R "^From: .*"$name".* " $year-$month.txt | wc | awk '{print $1}')
  # convert month names to numbers
  echo $(date --date="01 $month $year" +"%Y %m") $name $num
done
done
done

