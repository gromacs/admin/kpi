cd data
for year in 2020; do
  for month in January February March April May June July August September October November December; do
    wget https://mailman-1.sys.kth.se/pipermail/gromacs.org_gmx-users/${year}-${month}.txt.gz --no-check-certificate
  done
done

gunzip *.gz
