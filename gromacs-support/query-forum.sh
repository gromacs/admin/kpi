#!/bin/bash
phantomjs --web-security=no savepage.js "https://gromacs.bioexcel.eu/search?q=%40$1%20after%3A$2-$3-01%20before%3A$2-$3-31" > page.html

# have alook on how often post_number appears in the downloaded page
posts=$(grep -o "post_number" page.html | wc | awk '{print $1}')
# posts are counted double by looking at post_number, thus divide by 2
echo $((posts/2))
